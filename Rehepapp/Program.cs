﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rehepapp
{
    class Program
    {
        
        // Ahvid proovivad kirjutada "Rehepapi"
        static void Main(string[] args)
        {
            int x  = 8; //Tähti sõnas
            
              for (int i = 1; i < 100001; i++)
            
              { 
                if (GetVoucherNumber(x) != "rehepapp")
                {
                    Console.WriteLine(GetVoucherNumber(x)+ " - katse: " + i);
                }
                else 
                {   Console.WriteLine("REHEPAPP");
                    Console.ReadKey();
                }
              }
              Console.WriteLine("\nMonkeys' couldn't make it");
            Console.ReadKey();
        }

        private static Random random = new Random();

        public static string GetVoucherNumber(int length)
        {
            var chars = "abdeghijklmnoprstuvõäöü"; //Eesti tähestik
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }

    }
    
}
